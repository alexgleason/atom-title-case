# Title-case Package

Properly title case selected text. Inspired by [John Gruber's script](http://daringfireball.net/2008/05/title_case) and made possible with [to-title-case](https://www.npmjs.org/package/to-title-case).

![A preview of the package](http://cl.ly/image/1r1K1Q1x200K/title-case.gif)
